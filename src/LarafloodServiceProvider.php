<?php

namespace Cdonut\Laraflood;

use Illuminate\Support\ServiceProvider;

class LarafloodServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('laraflood', function ($app) {
            return new Laraflood;
        });
    }
}