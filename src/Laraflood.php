<?php

namespace Cdonut\Laraflood;

use Carbon\Carbon;

class Laraflood
{

    /**
     * @param $identity
     * @return bool
     */
    public function checkOnly($identity = 'ip', $action = 'default', $maxAttempts = 5)
    {
        if (!is_numeric($maxAttempts)) return;
        if ($identity == 'ip') $identity = $this->getrealip();

        $key = 'lf:' . $identity . ':' . $action;

        if (!\Cache::has($key))
            return true;
        $count = \Cache::get($key)['attempts'];
        if (!$count || !is_numeric($count))
            return true;

        # Check actions count per `cache_time` minutes
        if ($count < $maxAttempts)
            return true;

        return false;
    }

    public function check($identity = 'ip', $action = 'default', $maxAttempts = 5, $seconds = 300)
    {

        if (!is_numeric($maxAttempts) || !is_numeric($seconds)) return;
        # add attempt
        $this->addAttempt($identity, $action, $seconds);
        # check
        return $this->checkOnly($identity, $action, $maxAttempts);

    }

    /**
     * pushes identityify key to cache
     *
     * @param     $identity
     * @param     $action
     * @param int $minutes
     */
    public function addAttempt($identity = 'ip', $action = 'default', $seconds = 300)
    {
        if ($identity == 'ip') $identity = $this->getrealip();
        if (!is_numeric($seconds)) return;

        $data = array('action' => $action, 'attempts' => 1, 'expiration' => Carbon::now()->addSeconds($seconds));

        $key = 'lf:' . $identity . ':' . $action;

        if (\Cache::has($key)) {
            $c = \Cache::get($key);
            $updated_data = array('action' => $action, 'attempts' => $c['attempts'] + 1, 'expiration' => $c['expiration']);
            \Cache::put($key, $updated_data, $seconds);
        } else {
            \Cache::put($key, $data, $seconds);
        }
    }

    /**
     * Gets the time left from the cache
     *
     * @param     $identity
     * @param     $action
     */
    public function secondsLeft($identity = 'ip', $action = 'default')
    {
        if ($identity == 'ip') $identity = $this->getrealip();
        $key = 'lf:' . $identity . ':' . $action;

        if (!\Cache::has($key))
            return;

        $now = Carbon::now();
        $expiration = Carbon::parse(\Cache::get($key)['expiration']);
        return $expiration->diffInSeconds($now);
    }

    /**
     * Gets the cache item
     *
     * @param     $identity
     * @param     $action
     */
    public function get($identity = 'ip', $action = 'default')
    {
        if ($identity == 'ip') $identity = $this->getrealip();
        $key = 'lf:' . $identity . ':' . $action;
        return \Cache::get($key);

    }

    /**
     * Get client real ip
     *
     */
    private function getrealip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    /**
     * Forget an item from the cache to ignore ir¡t
     *
     * @param     $identity
     * @param     $action
     */
    public function clear($identity = 'ip', $action = 'default')
    {
        if ($identity == 'ip') $identity = $this->getrealip();
        $key = 'lf:' . $identity . ':' . $action;
        \Cache::forget($key);
        return true;
    }

}
